const express = require('express');
const playgroundHandler = require('../handlers/playground-handler.js');
const middleware = require('../middleware/authMiddleware');
const router = express.Router();

// POST endpoint for adding a new course
router.post('/create_playground', middleware.checkNormalToken, playgroundHandler.createPlayground);

// POST endpoint for updateing the playground
router.post('/update_playground', middleware.checkNormalToken, playgroundHandler.updatePlayground);

// GET endpoint for retreaving a playground
router.get('/get_playground/:id', middleware.checkNormalToken, playgroundHandler.getPlayground);

// GET endpoint for retreaving all the user's playgrounds
router.get('/get_playgrounds/:username', middleware.checkNormalToken, playgroundHandler.getPlaygrounds);

router.post('/delete_playground/:uid/:pid', middleware.checkNormalToken, playgroundHandler.deletePlayground);

router.post('/update_last_modified/:id', middleware.checkNormalToken, playgroundHandler.updateLastModified);
module.exports = router;