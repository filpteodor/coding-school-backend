const express = require('express');
const router = express.Router();

const middleware = require('../middleware/authMiddleware');
const challengesHanlder = require('../handlers/challengesHnadler');


router.post('/create_daily', middleware.checkAdminToken, challengesHanlder.createDaily);
router.get('/get_daily', middleware.checkAdminToken, challengesHanlder.getDaily);

router.post('/create_weekly', middleware.checkNormalToken, challengesHanlder.createWeekly);
router.get('/get_weekly', middleware.checkNormalToken, challengesHanlder.getWeekly);

module.exports = router;