const express = require('express');
const router = express.Router();

const middleware = require('../middleware/authMiddleware');
const account = require('../handlers/account');

router.post('/validate/:userId', account.validateAccount);
router.post('/requestResetPassword', account.requestResetPassword);
router.post('/resetPassword', account.resetPassword);
router.post('/checkToken', account.validateToken);
router.post('/update_profile/:id', middleware.checkNormalToken, account.updateProfile);

module.exports = router;