const express = require('express');
const auth = require('../handlers/auth');
const middleware = require('../middleware/authMiddleware');
const router = express.Router();

router.post('/signin', auth.adminSignIn);
router.post('/create', auth.createAdmin);

module.exports = router;
