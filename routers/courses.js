const express = require('express');
const courseHandler = require('../handlers/courses.js');
const router = express.Router();
const middleware = require('../middleware/authMiddleware');

const multer = require('multer');
const upload = multer();

/**
 * GET API ENDPOINTS
 */

router.get('/get_course_with_progress/:courseId', middleware.checkNormalToken, courseHandler.getCourseWithProgress);
router.get('/get_courses', middleware.checkNormalToken, courseHandler.getCourses);
router.get('/get_questions/:courseId', middleware.checkNormalToken, courseHandler.getQuestions);
router.get('/get_course/:id', middleware.checkNormalToken, courseHandler.getCourseById);
router.get('/get_section/:sectionId', middleware.checkNormalToken, courseHandler.getSectionById)

/**
 * POST API ENDPOINTS
 */

router.post('/save_progress', middleware.checkNormalToken, courseHandler.saveProgress);
router.post('/add_question', middleware.checkNormalToken, courseHandler.addQuestion);
router.post('/add_question_reply', middleware.checkNormalToken, courseHandler.addQuestionReply);
router.post('/add_course', [upload.any(), middleware.checkAdminToken], courseHandler.addCourse);
router.post('/add_section', middleware.checkAdminToken, courseHandler.addSection);
router.post('/add_lecture', middleware.checkAdminToken, courseHandler.addLecture);

module.exports = router;