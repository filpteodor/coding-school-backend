const express = require('express');
const auth = require('../handlers/auth');
const middleware = require('../middleware/authMiddleware');

const router = express.Router();

router.post('/signin', auth.signIn);
router.post('/signup', auth.signUp);
router.post('/check_token', auth.validateToken);
router.post('/get_user_by_token', middleware.checkNormalToken, auth.getUserByToken);
// the user does not have to be logged in to enter the beta access code
router.post('/find_access_code', auth.findAccessCode);
router.post('/disable_access_code', auth.disableAccessCode)

module.exports = router;
