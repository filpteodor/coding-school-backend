class StaticLecture {
    constructor(title, sublectures, type) {
        this.title = title;
        this.sublectures = sublectures;
        this.type = type;
    }
}

class SubLecture {
    constructor(type, content) {
        this.type = type;
        this.content = content;
    }
}

class InteractiveLecture {
    constructor(type, validator, task, hint) {
        this.type = type;
        this.validator = validator;
        this.task = task;
        this.hint = hint;
    }
}