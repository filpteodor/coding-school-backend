const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const replySchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },

    reply: {
        type: String,
        required: true,
        minlength: 1
    },

    createdAt: {
        type: Date,
        required: true
    }
})

module.exports = replySchema;