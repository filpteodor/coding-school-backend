const mongoose = require('mongoose');
const replySchema = require('./reply');

const Schema = mongoose.Schema;
const questionSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },

    course: {
        type: Schema.Types.ObjectId,
        ref: 'course',
        required: true
    },

    title: {
        type: String,
        required: true,
        minlength: 5
    },

    description: {
        type: String,
        required: true,
        minlength: 5
    },

    createdAt: {
        type: Date,
        required: true
    },

    replies: [replySchema]
})

const Question = mongoose.model('question', questionSchema);

module.exports = Question;