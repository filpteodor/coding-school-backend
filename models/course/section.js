const mongoose = require('mongoose');

var sectionSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true,
        sparse: true,
        // index: false
    },

    /* This is a Mixed schema type definiton
     * This array should store paragraphs or code examples
     * related to the lecture's topic.
     */
    lectures: [{}]
});

const Section = mongoose.model('section', sectionSchema);

module.exports = {
    sectionSchema: sectionSchema,
    Section: Section
};