const mongoose = require('mongoose');
const { sectionSchema } = require('./section.js');

const Schema = mongoose.Schema;
// creating the mongoose course schema
var courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true,
        unique: true
    },
    topic: {
        type: String,
        required: true
    },
    difficulty: {
        type: Number,
        required: true,
    },
    /**
     * A simple array of @var sectionSchema documents.
     * This are the sections of the course.
     */
    sections: [sectionSchema],

    questions: [{
        type: Schema.Types.ObjectId,
        ref: 'question'
    }]
});

const Course = mongoose.model('course', courseSchema);

module.exports = Course;