const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const { promisify } = require('util');

// playground model required in order for the populate function to work
const Playground = require('./playground');
const Schema = mongoose.Schema;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        match: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
        unique: true
    },

    firstName: {
        type: String,
    },

    lastName: {
        type: String
    },

    username: {
        type: String,
        required: true,
        minlength: 5,
        trim: true,
        // unique: true
    },

    password: {
        type: String,
        required: true,
        minlength: 8,
    },

    validated: {
        type: Boolean,
        default: false
    },

    resetPasswordToken: {
        type: String,
        default: null
    },

    playgrounds: [{ type: Schema.Types.ObjectId, ref: 'playground' }],
    progress: {
        type: Object,
        default: {}
    },

    questions: [{
        type: Schema.Types.ObjectId,
        ref: 'question'
    }]

})

userSchema.methods.hashPassword = function() {
    const saltRounds = 10;
    return new Promise((res, rej) => {
        bcrypt.hash(this.password, saltRounds, (err, hash) => {
            if (err)
                rej(err.message);

            this.password = hash;
            res();
        })
    })
}

userSchema.statics.saveProgress = function(userId, courseId, sectionId, completedLectures) {
    let path = `progress.${courseId}.sections.${sectionId}`;
    return this.findByIdAndUpdate(userId, {
        $set: {
            [path]: completedLectures
        }
    })
}

userSchema.methods.matchPassword = function(password) {
    return promisify(bcrypt.compare)(password, this.password);
}

const User = mongoose.model('user', userSchema);

module.exports = User;