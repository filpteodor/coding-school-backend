const mongoose = require('mongoose');

const weeklyChallengeSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    task: {
        type: String,
        required: true,
        unique: true
    }
})

const WeeklyChallenge = mongoose.model('weekly', weeklyChallengeSchema);

module.exports = WeeklyChallenge;