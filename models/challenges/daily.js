const mongoose = require('mongoose');

const dailyCodingProblem = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    task: {
        type: String,
        required: true,
        unique: true
    },
    hint: {
        type: String,
        required: true
    },
    validator: {
        type: String,
        required: true,
        unique: true
    }
})

const DailyCodingProblem = mongoose.model('daily', dailyCodingProblem);

module.exports = DailyCodingProblem;