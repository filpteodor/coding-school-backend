const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const { promisify } = require('util');

const adminSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 5,
        trim: true,
        // unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
    },
})

adminSchema.methods.hashPassword = function() {
    const saltRounds = 10;
    return new Promise((res, rej) => {
        bcrypt.hash(this.password, saltRounds, (err, hash) => {
            if (err)
                rej(err.message);

            this.password = hash;
            res();
        })
    })
}

adminSchema.methods.matchPassword = function(password) {
    return promisify(bcrypt.compare)(password, this.password);
}

const Admin = mongoose.model('admin', adminSchema);

module.exports = Admin;