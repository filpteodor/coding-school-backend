const mongoose = require('mongoose');

const playgroundSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: false
    },
    description: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true
    },
    code: [{}],
    environmentType: {
        type: Number,
        required: true
    },
    lastModified: {
        required: false,
        unique: false,
        type: Date
    }
});

const Playground = mongoose.model('playground', playgroundSchema);

// exporting the playgroudn mongoose model
module.exports = Playground;