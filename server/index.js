require('dotenv').config();
require('../db/mongoose');

const express = require('express');
const {
    promisify
} = require('util');
const bodyParser = require('body-parser');
const cors = require('cors');

// requiring express routers 
const authRouter = require('../routers/auth');
const coursesRouter = require('../routers/courses.js');
const accountRouter = require('../routers/account');
const playgroundRouter = require('../routers/playground-router');
const adminRouter = require('../routers/admin-router');
const challengesRouter = require('../routers/challenges');

var app = express();

// serving the course images and the user profile pictures
app.use(express.static('public'));

// adding basic node express middleware
app.use(bodyParser.json());
app.use(cors());

// check routes/auth.js
app.use('/auth', authRouter);
app.use('/account', accountRouter);

// check routes/courses.js
app.use('/courses', coursesRouter);
// check routes/account.js
app.use('/account', accountRouter);
// check routes/playground-router.js
app.use('/playground', playgroundRouter);
// check routes/admin-router.js
app.use('/admin', adminRouter);
// check routes/challenges.js
app.use('/challenges', challengesRouter);



// Starting the server
const startServer = async() => {
    var PORT = process.env.PORT || 8081;
    await promisify(app.listen).bind(app)(PORT);
    console.log(`Listening on port ${PORT}`);
}

// starting the server
startServer();