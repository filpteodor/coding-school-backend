const tokenHandler = require('../utils/token');
const res = require('../utils/response');

const checkNormalToken = (request, response, next) => {
    let token = request.headers['x-access-token'] || request.body['x-access-token'];

    if (!token || token == 'undefined') {
        console.log('Token undefined')
        return res.middlewareInvalidToken(response);
    } else {
        tokenHandler.verify(token).then((decoded) => {
            next();
        }).catch((error) => {
           console.log('an error has occured')
            return response.status(401).send(error);
        })
    }
}

// checking if the token that the user sent belongs to an admin account
const checkAdminToken = async(request, response, next) => {
    let token = request.headers['x-access-token'] || request.body['x-access-token'];
    if (!token || token == 'undefined') {
        return res.middlewareInvalidToken(response);
    } else {
        tokenHandler.verify(token).then((decoded) => {
            if (decoded.isAdmin) {
                next();
            } else {
                return res.middlewareInvalidToken(response);
            }
        }).catch((error) => {
            return response.status(401).send(error);
        })
    }
}

module.exports = {
    checkNormalToken,
    checkAdminToken
}
