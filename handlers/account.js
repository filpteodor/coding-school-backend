const UserService = require('../services/userService');
const response = require('../utils/response');
let userService = new UserService();

const validateAccount = async (req, res) => {
    let userId = req.params.userId;
    if (!userId)
        return response.userIdRequired(res);
    try {
        let user = await userService.getUserById(userId);
        if (!user.validated) {
            await userService.validateAccount(user);
            return response.accountActivatedSuccessfully(res);
        } else {
            return response.accountAlreadyActivated(res);
        }
    } catch (err) {
        response.somethingWentWrong(res);
    }
}

const requestResetPassword = async (req, res) => {
    let email = req.body.email;
    try {
        await userService.requestResetPassword(email);
        return response.waitForResetPasswordEmail(res);
    } catch (err) {
        response.somethingWentWrong(res);
    }
}

const resetPassword = async (req, res) => {
    let token = req.body.resetToken;
    let password = req.body.password;

    try {
        await userService.resetPassword(token, password);
        return response.passwordResetSuccessful(res);
    } catch (err) {
        return response.somethingWentWrong(res);
    }
}

const validateToken = async (req, res) => {
    let token = req.body.token;
    if (!token)
        return response.tokenRequired(res);

    let isValid = await userService.isValidToken(token);
    if (isValid)
        return response.validToken(res);
    else
        return response.invalidToken(res);
}

const updateProfile = async (req, res) => {
    let userId = req.params.id;
    let updatedUserObject = req.body.updatedUserObject;

    let userToBeUpdated = await userService.getUserById(userId);

    userToBeUpdated.firstName = updatedUserObject.firstName;
    userToBeUpdated.lastName = updatedUserObject.lastName;
    userToBeUpdated.email = updatedUserObject.email;
    userToBeUpdated.username = updatedUserObject.username;

    userToBeUpdated.save().then((doc) => {
        if (doc) {
            response.updatedProfile(res);
        } else {
            response.canNotUpdateProfile(res)
        };
    }).catch((err) => {
        response.canNotUpdateProfile(res)
    })
}

module.exports = {
    validateAccount,
    requestResetPassword,
    resetPassword,
    validateToken,
    updateProfile
}
