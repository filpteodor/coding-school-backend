const CourseService = require('../services/courseService.js');
const UserService = require('../services/userService.js');
const courseService = new CourseService();
const userService = new UserService();
// const fs = require('fs');
const storage = require('../utils/fileUpload');

const addSection = async (request, response) => {

    let courseId = request.body.courseId;
    let sectionObject = request.body.sectionObject;

    try {
        let newSection = await courseService.addSection(courseId, sectionObject)
        if (newSection) {
           console.log('added section');
            response.status(200).send(newSection);
        } else {
           console.log('Section not added')
            response.status(400).send('error');
        }
    } catch (error) {
      console.log(error);
        response.status(400).send('Error');
    }
}

const addLecture = async (request, response) => {
    let courseId = request.body.courseId;
    let sectionId = request.body.sectionId;
    let lecture = request.body.lecture;

    try {
        let updatedCourse = await courseService.addLecture(courseId, sectionId, lecture);
        response.status(200).send(updatedCourse);
    } catch (error) {
        response.status(500).send(error)
    }
}

const addCourse = async (request, response) => {
    try {
        let imageFile = request.files[0];
        let courseObject = JSON.parse(request.body.courseData);

        // saving the course and uploading the image
        let newCourse = await courseService.createCourse(courseObject);
        storage.uploadFile('uploads', newCourse._id, imageFile);

        response.status(200).send(newCourse);
    } catch (error) {
        console.log(error);
        response.status(400).send('There was an error');
    }
}

const getSectionById = async (request, response) => {
    let sectionId = request.params.sectionId;
    try {
        let currentSection = await courseService.getSectionById(sectionId);
        response.status(200).send(currentSection);
    } catch (error) {
        response.status(400).send(error);
    }
}

const getCourses = async (request, response) => {
    try {
        let courses = await courseService.getCourses();
        if (courses && courses.length) {
            response.status(200).send(courses);
        }
    } catch (error) {
        response.status(200).send('error from server');
    }
};

const getCourseById = async (request, response) => {

    let courseId = request.params.id;

    try {
        let course = await courseService.getCourseById(courseId);
        if (course) {
            response.status(200).send(course);
        } else {
            response.status(400).send('Course not found');
        }
    } catch (error) {
        response.status(400).send('Course not found');

    }
}

const saveProgress = async (req, res) => {
    let token = req.body['x-access-token'];
    let courseId = req.body.courseId;
    let sectionId = req.body.sectionId;
    let completedLectures = req.body.completedLectures;
    try {
        let userId = (await userService.getUserIdByToken(token));
        if (userId) {
            await userService.saveProgress(userId, courseId, sectionId, completedLectures);
            res.status(200).send();
        }
    } catch (err) {}
}

const getCourseWithProgress = async (req, res) => {
    let token = req.headers['x-access-token'];
    let courseId = req.params.courseId;
    try {
        let user = await userService.getUserByToken(token);
        if (user) {
            let courseProgress = await userService.getCourseProgress(user, courseId);
            let course = await courseService.getCourseById(courseId);
            let userScore = userService.getUserScore(user);
            res.send({
                course,
                courseProgress,
                userScore
            });
        }
    } catch (err) {}
}

const addQuestion = async (req, res) => {
    let token = req.body['x-access-token'];
    let courseId = req.body.courseId;
    let title = req.body.title;
    let description = req.body.description;

    let user = await userService.getUserByToken(token);
    let course = await courseService.getCourseById(courseId);

    await courseService.addQuestion({
        author: user,
        course,
        title,
        description
    });
    res.send();
}

const getQuestions = async (req, res) => {
    let courseId = req.params.courseId;
    let questions = await courseService.getCourseQuestions(courseId);
    res.send(questions);
}

const addQuestionReply = async (req, res) => {
    let questionId = req.body.questionId;
    let token = req.body['x-access-token'];
    let reply = req.body.reply;

    let userId = (await userService.getUserByToken(token)).id;
    await courseService.addQuestionReply(userId, questionId, reply);
    res.send();
}

module.exports = {
    addCourse,
    getCourseById,
    getSectionById,
    getCourseWithProgress,
    saveProgress,
    addQuestion,
    getQuestions,
    addQuestionReply,
    addSection,
    addLecture,
    getCourses
}
