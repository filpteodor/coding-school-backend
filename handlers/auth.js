const UserService = require('../services/userService');
const AdminService = require('../services/adminService');
const response = require('../utils/response');
const tokenHandler = require('../utils/token')

// services instances
let userService = new UserService();
let adminService = new AdminService();

const signIn = async (req, res) => {

    let email = req.body.email;
    let password = req.body.password;
    let expirationTime = req.body.rememberMe ? '365d' : '24h';

    if (email && password) {
        let user = await userService.getUserByEmail(email);
        console.log()
        if (!user)
            return response.invalidEmail(res);

        let match = await userService.matchPassword(user, password);
        if (!match)
            return response.invalidPassword(res);

      if (!user.validated) {
         return response.accountActivationRequired(res);
      }

      // generating a token
      let token = tokenHandler.generate({
         id: user.id,
         isAdmin: false
      }, expirationTime);

      // getting the username so that it can be later savedin localStorage
      let username = user.username;

      // sending the successful response to frontend
      return response.authenticationSuccessful(res, token, username);

   } else {
      return response.invalidUsernameOrPassword(res);
   }
}

const signUp = async (req, res) => {

    let email = req.body.email;
    let username = req.body.username;
    let password = req.body.password;
    let firstName = req.body.firstName;
    let lastName = req.body.lastName;

    if (username && password && email) {
        try {

            if (await userService.getUserByEmail(email)) {
                return response.emailAlreadyInUse(res);
            }

            if (await userService.getUserByUsername(username)) {
                return response.usernameAlreadyInUse(res);
            }

            await userService.signUpWithEmailVerification({
                email,
                username,
                password,
                firstName,
                lastName
            });

            return response.waitForVerificationEmail(res);
        } catch (err) {
            return response.somethingWentWrong(res);
        }
    }
}

const getUserByToken = async function (request, res) {
    let token = request.body['x-access-token'];

   let user = await userService.getUserByToken(token);

   if (user) {
      response.foundUserByToken(res, user);
   } else {
      response.didNotFindUser();
   }
}

const adminSignIn = async function(request, response) {
   let username = request.body.username;
   let password = request.body.password;

   if (username && password) {
      let admin = await adminService.getUserByUsername(username);

      if (admin) {
         // this means we haveethe correct username
         let match = await admin.matchPassword(password);

         if (match) {
            let expirationTime = '24h';

            // generating a token
            let token = tokenHandler.generate({
               id: admin.id,
               isAdmin: true
            }, expirationTime);

            // sending the succesfull resposne
            response.status(200).send(token);
         } else {
            response.status(444).send('Please enter the correct password.');
         }
      } else {
         response.status(400).send('The username does not exist');
      }
   } else {
      response.status(400).send('Please fill in all the fields');
   }
}

const createAdmin = async function(request, response) {
   let adminData = request.body;
   let adminDoc = await adminService.createAdmin(adminData);

   if (adminDoc) {
      response.status(200).send(adminDoc);
   } else {
      response.status(400).send('Something went wrong');
   }
}

const validateToken = function(request, response) {
   let token = request.body.token;
   userService.isValidToken(token).then((decodedToken) => {
      response.status(200).send(decodedToken);
   }).catch((error) => {
      response.status(400).send(error);
   })
}

const findAccessCode = async function(request, res) {

   let submitedAccessCode = request.body.code;
   let accessCode = await userService.findAccessCode(submitedAccessCode);

   if(accessCode == null ) {
      response.sendFail(res);
   } else {
      res.status(200).send({success: true, data: accessCode});
   }
}

const disableAccessCode = async function(request, res) {
   let codeToBeDisabled = request.body.code;
   let disabledCode = await userService.disableAccessCode(codeToBeDisabled);
   if(disabledCode.used = true) response.sendSuccess(res);
   else response.sendFail(res);
}

module.exports = {
   signIn,
   signUp,
   adminSignIn,
   createAdmin,
   validateToken,
   getUserByToken,
   findAccessCode,
   disableAccessCode
}
