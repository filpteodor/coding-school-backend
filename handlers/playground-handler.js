const PlaygroundService = require('../services/playgroundService');
const UserService = require('../services/userService');

let playgroundService = new PlaygroundService();
let userService = new UserService();

const responseUtil = require('../utils/response.js');

const updateLastModified = async function(request, response) {

}

const updatePlayground = async function(request, response) {
    let id = request.body.id;
    let newCode = request.body.newCode;
    let lastModified = request.body.lastModified;

    let newPlayground = await playgroundService.updatePlayground(id, newCode, lastModified);

    if (newPlayground) {
        response.status(200).send(newPlayground);
    } else {
        response.status(400).send();
    }

}

/**
 *
 * @param {*} request
 * @param {*} response\
 *
 * Retrives the playground basesd on it's id.
 */

const getPlayground = async function(request, response) {

    let playground = await playgroundService
        .getPlaygroundById(request.params.id);

    if (playground) {
        response.status(200).send(playground);
    } else {
        response.status(400).send('We could not find your playground');
    }
}

/**
 *
 * @param {*} request
 * @param {*} response\
 *
 * Ends up creating a new playground and also saving
 * it's id in the current logged in user's playgrounds array.
 */

 const createPlayground = async function(request, response) {

     try {
       let playgroundData = request.body.playgroundData;
        playgroundData.lastModified = new Date();
        console.log('Playground data in handler is ' + playgroundData)
       let username = request.body.username;

       let playgroundWithTheSameTitle = await playgroundService.getPlaygroundByTitle(playgroundData.title);

       if (playgroundWithTheSameTitle != null) {
         responseUtil.playgroundAlreadyExists(response);
       } else {
         try {
            var playgroundInfo = playgroundService.createPlayground(playgroundData, username);
            responseUtil.sendSuccess(response);
         } catch (error) {
            responseUtil.sendFail(response);
         }
      }
     } catch (e) {
       console.log(e);
     }
 };

const deletePlayground = async function(request, response) {

    
    const userId = request.params.uid;
    const playgroundId = request.params.pid;


    // we first need to remove the playground's id from the user's 
    // playgrounds array
    const user = await userService.getUserById(userId);

    for(let i = 0; i < user.playgrounds.length; i++) {
        if(user.playgrounds[i] == playgroundId) user.playgrounds.splice(i, 1);
    }
    
    user.save();

    try {
        var deletedPlayground = playgroundService.deletePlayground(playgroundId);
        // console.log('Deleted playground is ' + deletedPlayground);
        if(deletedPlayground != null) responseUtil.sendSuccess(response);
        else responseUtil.sendFail(response);
    } catch (error) {
        console.log(error);
        responseUtil.sendFail(response);
    }   
}

/**
 *
 * @param {*} request
 * @param {*} response
 * Ends up getting the user with the playgrounds array populated.
 */

const getPlaygrounds = async function(request, response) {

    let username = request.params.username;

    try {
        let playgrounds = await playgroundService.getPlaygrounds(username);

        if (playgrounds != null) {
            response.status(200).send(playgrounds);
        }

    } catch (error) {
        console.log(error)
        response.status(400).send(error);
    }
}

module.exports = {
    createPlayground,
    getPlaygrounds,
    getPlayground,
    updatePlayground,
    deletePlayground,
    updateLastModified
}
