const ChallengesService = require('../services/challengesService.js');
const challengesService = new ChallengesService();
const responseUtil = require('../utils/response.js');

const createDaily = async(request, response) => {
    try {
        let dailyObject = request.body.daily;
        await challengesService.createDaily(dailyObject);
        responseUtil.challengeCreated(response);
    } catch (error) {
        console.log(error);
        responseUtil.challengeNotCrated(response);
    }
};

const getDaily = async(request, response) => {
    try {
        let dailyCodingProblems = await challengesService.getDaily();
        responseUtil.challengesFetched(response, dailyCodingProblems);
    } catch (error) {
        console.log(error);
        responseUtil.cannotGetChallenges(response);
    }
};

const createWeekly = async(request, response) => {
    try {
        let weeklyObject = request.body.weekly;
        await challengesService.createWeekly(weeklyObject);
        responseUtil.challengeCreated(response);
    } catch (error) {
        responseUtil.challengeNotCrated(response);
    }
};

const getWeekly = async(request, response) => {
    try {
        let weeklyChallenges = await challengesService.getWeekly();
        responseUtil.challengesFetched(response, weeklyChallenges);
    } catch (error) {
        responseUtil.cannotGetChallenges(response);
    }
};

module.exports = {
    createDaily,
    getDaily,
    createWeekly,
    getWeekly
}