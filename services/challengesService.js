const Daily = require('../models/challenges/daily');
const Weekly = require('../models/challenges/weekly');

class ChallengeService {
    createDaily(dailyObject) {
        let daily = new Daily({...dailyObject })
        return daily.save();
    }

    getDaily() {
        return Daily.find();
    }

    createWeekly(weeklyObject) {
        let weekly = new Weekly({...weeklyObject })
        return weekly.save();
    }

    getWeekly() {
        return Weekly.find()
    }
}

module.exports = ChallengeService;