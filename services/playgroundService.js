const Playground = require('../models/playground.js');
const UserService = require('../services/userService');
let userService = new UserService();

class PlaygroundService {

    async getPlaygroundById(id) {
        return Playground.findOne({ _id: id });
    }

    async updatePlayground(id, newCode, lastModified) {
        let oldPlayground = await this.getPlaygroundById(id);

        // replacing the old code with the new one and also updating the last modified date
        oldPlayground.code = newCode;
        oldPlayground.lastModified = lastModified;

        // saving the new playground
        let newPlayground = await oldPlayground.save();
        console.log(newPlayground)
        return newPlayground;
    }

    //creates a playground
    async createPlayground(playgroundData, username) {

        // getting the current logged in  user 
        var user = await userService.getUserByUsername(username);

        // creating the new playground
        var playground = new Playground({...playgroundData, lastModified: new Date() });
        
        // we need the playground document so we can store 
        // the newly created playground's id in the user
        // playgrounds array
        var playgroundDocument = await playground.save();
    
        // adding the playground id in the user's array
        user.playgrounds.push(playgroundDocument._id);
        var updatedUserDoc = await user.save();

        if (playgroundDocument != null && updatedUserDoc != null) {
            return { playgroundDocument, updatedUserDoc }
        } else {
            throw 'Something went wrong'
        }
    }

    async deletePlayground(id) {
        Playground.findByIdAndRemove(id, function(error, doc) {
           if(error) throw error.message
           else return doc; 
        })
    }

    async getPlaygrounds(username) {

        console.log(username)

        // getting the current logged in user 
        let user = await userService.getUserByUsername(username);

        if (user) {
            // we only need the playgrounds array
            
            return user.playgrounds;
        } else {
            throw 'Something went wrong'
        }
    }

    async getPlaygroundByTitle(title) {
        return Playground.findOne({ title: title });
    }
}

module.exports = PlaygroundService;