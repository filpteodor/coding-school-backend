const Course = require('../models/course/course.js');
const {
    Section
} = require('../models/course/section');
const Question = require('../models/course/q&a/question');

class CourseService {

    getCourseById(id) {
        return Course.findById(id);
    }

    getSectionById(sectionId) {
        return Section.findOne({
            _id: sectionId
        })
    }

    async addLecture(courseId, sectionId, lecture) {
        let course = await this.getCourseById(courseId);
        let sectionIndex;

        course.sections.forEach((section, index) => {
            if (section.id == sectionId) {
                sectionIndex = index;
            }
        });

        course.sections[sectionIndex].lectures.push(lecture);
        let updatedCourse = await course.save();
        return updatedCourse;
    }

    async addSection(courseId, sectionObject) {

        // getting the current course
        let course = await this.getCourseById(courseId);
        // saving the new section in the database
        try {
            // getting the new section's db document
            let newSection = new Section(sectionObject);
            let newSectionDoc = await newSection.save();

            // adding the section to the course
            course.sections.push(newSectionDoc);
            await course.save();
            return newSectionDoc;
        } catch (error) {
            return null;
        }
    }

    // adding a new courseObject to the db
    createCourse(courseObj) {
        return Course.create({
            title: courseObj.title,
            description: courseObj.description,
            topic: courseObj.topic,
            difficulty: courseObj.difficulty,
        });
    }

    getCourses() {
        return Course.find();
    }

    async addQuestion({
        author,
        course,
        title,
        description
    }) {
        const question = new Question({
            author: author._id,
            course: course._id,
            title: title,
            description: description,
            createdAt: new Date(),
            replies: []
        });

        let savedQuestion = await question.save();

        author.questions.push(savedQuestion._id);
        course.questions.push(savedQuestion._id);

        await author.save();
        await course.save();
    }

    async getCourseQuestions(courseId) {

        let course = await this.getCourseById(courseId);
        await course.populate('questions').execPopulate();
        let questions = course.questions;

        for (let q of questions) {

            await q.populate({
                path: 'author',
                select: 'username'
            }).execPopulate();

            for (let i in q.replies)
                await q.populate({
                    path: `replies.${i}.author`,
                    select: 'username'
                }).execPopulate();
        }

        return questions;
    }

    async addQuestionReply(userId, questionId, reply) {
        let question = await Question.findById(questionId);
        question.replies.push({
            author: userId,
            reply,
            createdAt: new Date()
        })

        return question.save();
    }
}

module.exports = CourseService;
