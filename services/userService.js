const User = require('../models/user');
const Admin = require('../models/admin');
const AccessCode = require('../models/auth/AccessCode.js');

const validateAccountMail = require('../utils/verificationMailTemplate');
const resetPasswordMail = require('../utils/resetPasswordTemplate');
const tokenHandler = require('../utils/token');

// setting up mailgun
const MailgunService = require('../utils/mailgun');
const mailgunService = new MailgunService();

class UserService {

   // This function is used for getting the playgrounds associated
   // with the user. Will be called from playgrounds page.
   async getUserByUsername(username) {
      let user = await User.findOne({
         username
      }).populate('playgrounds');

      return user;
   }

   getUserByEmail(email) {
      return User.findOne({
         email
      });
   }

   async getUserById(userId) {
      let user = await User.findById(userId);
      if (!user) user = await Admin.findById(userId);
      return user;
   }

   matchPassword(user, password) {
      return user.matchPassword(password);
   }

   hashPassword(user) {
      return user.hashPassword();
   }

   async signUp(userData) {
      let user = new User({
         ...userData
      });
      await user.hashPassword();
      return user.save();
   }

   validateAccount(user) {
      user.validated = true;
      return user.save();
   }

   async requestResetPassword(email) {
      let user = await this.getUserByEmail(email);
      let token = tokenHandler.generate({
         id: user._id
      });
      user.resetPasswordToken = token;
      await user.save();
      mailgunService.sendMail({
         to: user.email,
         from: 'Codewaves Team <modure_rares@mrv-ti.com>',
         subject: 'Reset password',
         html: resetPasswordMail.getMail(token)
      });
   }

   async resetPassword(token, password) {

      let decoded = await tokenHandler.verify(token)

      let userId = decoded.id;
      let user = await this.getUserById(userId);

      if (!user)
         throw new Error('User was not found');

      if (!user.resetPasswordToken)
         throw new Error("User hasn't requested password reset");

      user.password = password;
      user.resetPasswordToken = null;

      await this.hashPassword(user);
      await user.save();
   }

   async signUpWithEmailVerification(userData) {
      let user = new User({
         ...userData
      });

      await user.hashPassword();
      await user.save();

      let emailData = {
         to: user.email,
         from: 'Codewaves Team <modure_rares@mrv-it.com>',
         subject: 'Account verification',
         text: 'You need to confirm your email to activate your account.',
         html: validateAccountMail.getMail(user._id)
      }

      mailgunService.sendMail(emailData);
   }

   async isValidToken(token) {
      if (!token || token == 'undefined') {
         return false;
      } else {
         tokenHandler.verify(token).then((decoded) => {
            return true;
         }).catch((error) => {
            return false;
         })
      }
   }

   findAccessCode(submitedAccessCode) {
      return AccessCode.findOne({code: submitedAccessCode});
   }

   disableAccessCode(codeToBeDisabled) {
      let code = AccessCode.findOne({code: codeToBeDisabled});
      code.used = true;
      return code.save();
   }

    async getUserByToken(token) {
        let userId = (await tokenHandler.verify(token)).id;
        return this.getUserById(userId);
    }

    async getUserIdByToken(token) {
        return (await tokenHandler.verify(token)).id;
    }

    async getCourseProgress(user, courseId) {
        if (!user.progress[courseId])
            return {
                sections: {}
            };
        return user.progress[courseId];
    }

    getUserScore(user) {
        if (!user.progress)
            return 0;
        let score = 0;
        const coursesIds = Object.keys(user.progress);

        coursesIds.forEach(id => {
            const sectionIds = Object.keys(user.progress[id].sections);
            sectionIds.forEach(sectionId => score += user.progress[id].sections[sectionId]);
        })

        return score;
    }

    saveProgress(userId, courseId, sectionId, completedSections) {
        return User.saveProgress(userId, courseId, sectionId, completedSections);
    }
}

module.exports = UserService;
