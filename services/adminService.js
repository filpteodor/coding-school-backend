const Admin = require('../models/admin');

class AdminService {

    getUserByUsername(username) {
        return Admin.findOne({ username: username });
    }

    async createAdmin(adminData) {
        let admin = new Admin({
            ...adminData
        });
        await admin.hashPassword();
        return admin.save();
    }
}

module.exports = AdminService;