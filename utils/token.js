const jwt = require('jsonwebtoken');

const generate = (payload, expiresIn) => {
    let token = jwt.sign(
        payload,
        process.env.jwt_secret, {
            expiresIn
        }
    )
    return token;
}

const verify = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, process.env.jwt_secret, (err, decoded) => {
            if (err)
                return reject(err);
            resolve(decoded);
        });
    })
}

module.exports = {
    generate,
    verify
}