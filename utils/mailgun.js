const mailgunApiKey = process.env.MAILGUN_KEY;
const domain = process.env.MAILGUN_DOMAIN;

var mailgun = require('mailgun-js')({
   apiKey: mailgunApiKey,
   domain: domain
});

class MailgunService {
   // this will be used to send email confirmation messages
   sendMail(data) {
      mailgun.messages().send(data, (error, body) => {
         console.log(body);
         if (error) {
            console.log(error.message)
            return {
               error: error.message
            }
         }
      });
   }

   sendAccessCode(code) {
      mailgun.messages().send(`This is the email with the code ${code}`, (error, body) => {
         if(error) return {
            error: error.message
         }
      })
   }
}

module.exports = MailgunService
