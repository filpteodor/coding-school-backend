const fs = require('fs');

function uploadFile(dirName, uploadFileName, file) {
    fs.writeFile(__dirname + `/../public/${dirName}/${uploadFileName}.jpg`, file.buffer, (err) => {
        if (err) {
            return null;
        }
    });
}

module.exports = {
    uploadFile
}