const generateCode = () => {
   var result = '';
   var characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWabcdefghijklmnopqrstuvw';
   var charactersLength = characters.length;
   for (var i = 0; i < 5; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

module.exports = {
   generateCode
}
