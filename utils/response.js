const invalidEmail = (res) => {
    res.send({
        success: false,
        message: 'Invalid email',
        invalidEmail: true
    });
}

const invalidPassword = (res) => {
    res.send({
        success: false,
        message: 'Invalid password',
        invalidPassword: true
    })
}

const accountActivationRequired = (res) => {
    res.send({
        success: false,
        accountNotActivated: true,
        message: 'Your account needs to be activated before you can sign in. Check your email in order to activate your account'
    })
}

const emailAlreadyInUse = (res) => {
    res.send({
        success: false,
        usedEmail: true,
        message: 'Email is already in use'
    });
}

const usernameAlreadyInUse = (res) => {
    res.send({
        success: false,
        usedUsername: true,
        message: "Username is already in use"
    });
}

const invalidUsernameOrPassword = (res) => {
    res.send({
        success: false,
        message: 'Invalid username or password'
    })
}

const authenticationSuccessful = (res, token, username) => {
    res.send({
        success: true,
        message: 'Authentication successful',
        token: token,
        username: username
    });
}

const somethingWentWrong = (res, err) => {
    res.send({
        success: false,
        message: "Something went wrong, please try again later!"
    })
}

const waitForVerificationEmail = (res) => {
    res.send({
        success: true,
        message: 'You will receive a verification email',
    })
}

const accountAlreadyActivated = (res) => {
    res.send({
        success: false,
        alreadyActivated: true,
        message: 'Your account has already been activated'
    })
}

const accountActivatedSuccessfully = (res) => {
    res.send({
        success: true,
        message: 'Your account was activated successfully'
    })
}

const userIdRequired = (res) => {
    res.send({
        success: false,
        message: 'You need to provide a user id'
    });
}

const waitForResetPasswordEmail = (res) => {
    res.send({
        success: true,
        message: 'Check your email to reset your password'
    });
}

const passwordResetSuccessful = (res) => {
    res.send({
        success: true,
        message: 'Password was resetted successfully'
    });
}

const tokenRequired = (res) => {
    res.send({
        success: true,
        message: 'Token is required'
    })
}

const validToken = (res) => {
    res.send({
        success: true,
        isValid: true
    })
}

const invalidToken = (res) => {
    res.send({
        sucess: true,
        isValid: false
    })
}

const challengeCreated = (response) => {
    response.send({
        success: true,
    })
};

const challengeNotCrated = (response) => {
    response.send({
        success: false,
        message: 'Daily could not be created, please try again later.'
    })
};

const challengesFetched = (response, problems) => {
    response.send({
        success: true,
        problems: problems
    })
}

const cannotGetChallenges = (response) => {
    response.send({
        success: false,
        message: 'Daily problems could not be fetched, please try again later.'
    })
};

const middlewareInvalidToken = (response) => {
    response.status(401).send({
        success: true,
        invalidToken: true
    })
}

const foundUserByToken = (response, userData) => {
    response.send({
        success: true,
        user: userData
    })
}

const didNotFindUser = (response) => {
    response.send({
        success: false,
    })
}

const updatedProfile = (response) => {
    response.send({
        success: true
    })
}

const canNotUpdateProfile = (response) => {
    response.send({
        success: false,
        message: 'Something went wrong whila updating your profile. Please try again later.'
    })
}

const sendPlaygrounds = (response, playgrounds) => {
    response.send({
        success: true,
        playgrounds: playgrounds
    })
}

const playgroundAlreadyExists = (response, playgrounds) => {
    response.send({
        success: false,
    })
}

const sendSuccess = (response, playgrounds) => {
    response.send({
        success: true,
    })
}

const sendFail = (response) => {
    response.send({
        success: false
    })
}

const successWithDat = (response, data) => {
   response.send({
      success: true,
      data: data
   })
}

const couldNotGetPlaygrounds = (response) => {
    response.send({
        success: false,
        message: 'Something went wrong, please try again later'
    })
}

const noPlaygroundsCreated = (response) => {
    response.send({
        success: true,
        noPlaygrounds: true
    })
}

module.exports = {
    sendFail,
    sendSuccess,
    playgroundAlreadyExists,
    sendPlaygrounds,
    noPlaygroundsCreated,
    couldNotGetPlaygrounds,
    canNotUpdateProfile,
    updatedProfile,
    didNotFindUser,
    foundUserByToken,
    invalidEmail,
    accountActivationRequired,
    invalidPassword,
    invalidUsernameOrPassword,
    authenticationSuccessful,
    somethingWentWrong,
    waitForVerificationEmail,
    accountAlreadyActivated,
    accountActivatedSuccessfully,
    userIdRequired,
    waitForResetPasswordEmail,
    passwordResetSuccessful,
    tokenRequired,
    validToken,
    invalidToken,
    emailAlreadyInUse,
    usernameAlreadyInUse,
    challengeCreated,
    challengeNotCrated,
    cannotGetChallenges,
    challengesFetched,
    middlewareInvalidToken
}
