const getMail = (token) => {
    return `<div style="font-family: Arial, Helvetica, sans-serif; color:#403865">
    <a href="http://localhost:8082/#" style="color: #666;
                font-size: .9rem;
            ">
            
                Need Help or Questions?
            </a>
    <img style="display: block; margin: 0 auto; padding: 3rem" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/01/kqxjt8mLHSCrJQF1p63wWXVd/all/images/t8-img1.png"/>
 
  <form method="GET" action="http://localhost:8080/signin/forgot/recover/${token}">
   <h2 style="text-align:center; letter-spacing: 1px; margin-top: 1rem; width: 100%">Forgot your password?</h2>
  <p style="color: light-grey; font-weight: 700; text-align:center; padding:1.5rem; line-height: 2rem; font-size: 1rem; width:60%; margin: 0 auto" > Hello, <br>
We received a request to reset the password associated with this e-mail address. Please, click the button below to create a new password at {{placehoder}}.</p>
     <button type="submit" style="padding: 1rem 1.5rem !important; border-radius: .5rem; border:none; background: #403865; color: white;
                margin: 0 auto; display:block; font-size: 1.2rem; font-weight:400;
            "><span style="margin: 20px">Reset Password</span></button>
  </form>
</div>`;
}

module.exports = {
    getMail
}